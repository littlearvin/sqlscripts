USE ModernWays;
CREATE TABLE Student(
Voornaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Familienaam VARCHAR(100) CHAR SET utf8mb4,
StudentNummer INT AUTO_INCREMENT PRIMARY KEY
);

CREATE TABLE Opleiding (
Naam VARCHAR(100) CHAR SET utf8mb4 PRIMARY KEY NOT NULL
);

CREATE TABLE Lector(
Naam VARCHAR(50) CHAR SET utf8mb4 NOT NULL,
FamilieNaam VARCHAR(50) CHAR SET utf8mb4 NOT NULL,
LectorNummer INT AUTO_INCREMENT PRIMARY KEY NOT NULL
);

CREATE TABLE Vak(
Naam VARCHAR(50) CHAR SET utf8mb4 PRIMARY KEY NOT NULL
);

/* Key systeem aanmaken */

ALTER TABLE Student /* Waar dat de key moet steken */
ADD COLUMN Opleiding_Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL, /* Var naam gaat dienen als foreign key verwijzing //Heeft een kolom nodig verwijst naar Table - Variable*/
ADD COLUMN Semester TINYINT UNSIGNED NOT NULL, /* Dit word er bij gehaalt als de sleutel word opgeroepen*/
ADD CONSTRAINT fk_Student_Opleiding FOREIGN KEY (Opleiding_Naam) REFERENCES Opleiding(Naam);


CREATE TABLE LectorGeeftVak

CREATE TABLE OpleidingsOnderdeel