USE ModernWays;

/* 0525__Oefening.sql, schrijf een script dat de zin "X is de naam van een hond" produceert voor elke hond in de database. 
De resultaten van je opdracht worden weergegeven in een tabel met één kolom, met als hoofding "Bewering". */
/* EErst de actie schrijven dan van waar etc. */
/*ALTER Huisdieren ADD COLUMN "Bewering" WHERE Soort = "hond";*/ 

SELECT /* Veranderd niets, dus as bewering is tijdelijk niet definitief */
	Concat(naam, "is de naam van een hond") AS Bewering    
FROM 
	Huisdieren
WHERE
	soort = "hond";
