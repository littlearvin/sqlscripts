USE `Modernways`;


ALTER TABLE Baasjes ADD COLUMN Huisdieren_id INT, 
ADD CONSTRAINT fk_baasjes_huisdieren FOREIGN KEY (Huisdieren_id) REFERENCES Huisdieren(id);