USE ModernWays;

SET SQL_SAFE_UPDATES = 0;

/* 0524__Oefening.sql, dat alle honden van Thaïs en katten van Truus wist.*/
/*1st actie zeggen*/
DELETE FROM Huisdieren WHERE Soort = "Hond" AND Baasje = "Thaïs" OR Soort = "Kat" AND Baasje = "Truus";


SET SQL_SAFE_UPDATES = 1;