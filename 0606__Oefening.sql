USE Modernways;

SELECT Leden.Voornaam, Boeken.Titel
FROM Leden
     INNER JOIN Uitleningen ON Leden_Id = Leden.Id 
     INNER JOIN Boeken ON Boeken_Id = Boeken.Id