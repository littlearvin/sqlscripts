Use Modernways; 
SET SQL_SAFE_UPDATES = 0;
ALTER TABLE 
	Huisdieren 
ADD COLUMN Geluid VARCHAR(20) CHARSET utf8mb4;
UPDATE Huisdieren SET Geluid = 'WAF!' WHERE Soort = 'Hond'; /* Update huisdieren, is selecteren wat uptedaten. Dan SET = selecteren in table */
UPDATE Huisdieren SET Geluid = 'Miauw' WHERE Soort = 'Kat';
SET SQL_SAFE_UPDATES = 1;






